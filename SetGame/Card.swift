//
//  Card.swift
//  SetGame
//
//  Created by candela on 2019/4/18.
//  Copyright © 2019年 candela. All rights reserved.
//

import Foundation

struct Card {
    
    var description: String {
        return ""
    }
    
    let numberOfShape: NumberOfShape
    let shape: Shape
    let shading: Shading
    let color: Color
    
    static func ==
    
    enum NumbeOfShape: Int, CaseIterable, CustomStringConvertible {
        var description: String {
            return self.rawValue
            
        }
        case one
        case two
        case three
    }
    
    enum Shading: CaseIterable{
        var description: String {
            return self.rawValue
            
        }
        case solid, striped, open
    }
    
    enum Shape: CaseIterable {
        var description: String {
            return self.rawValue
            
        }
        case diamond, squiggle, stadium
    }
    
    enum Color: CaseIterable {
        var description: String {
            return self.rawValue
            
        }
        case red, green, purple
    }
    
    
    
}
