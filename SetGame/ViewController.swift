//
//  ViewController.swift
//  SetGame
//
//  Created by candela on 2019/4/18.
//  Copyright © 2019年 candela. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var game = Game()

    @IBAction func clickCard(_ sender: UIButton) {
        print("card is clicked, title: \(String(describing: sender.currentTitle))")
    }
    
    @IBOutlet func clickNewGame(_ sender: UIButton) {
        game = Game()
        buttonsToCards = [UIButton: Card]()
        cardButtons.shuffle()
        initGame()
    }
    
    func initGame() {
        cardButtons.shuffle()
    }
    
    @IBOutlet var cardButtons: [UIButton]! {
        didSet {
            cardButtons.shuffle()
            game.initShowingCards(numberOfCards: 12)

            
            for i in 0..<cardButtons.count {
                let card = game.showingCards[i]
                let button = cardButtons[i]
                buttonsToCards[button] = card
                let attributedContent = getAttributedStringFromCard(card)
                button.setAttributedTitle(attributedContent, for:  UIControl.State.normal)
            }
        }
    }
    
    var buttonsToCards = [UIButton: Card]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for i in 0...5 {
            print("the \(i)th card: \(game.cards[i])")
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
}

extension ViewController {
    func getAttributedStringFromCard(_ card: Card) -> NSAttributedString {
        let contentString = getStringFromCard(Card)
        let color = getUIColorFromCard(Card)
        let attribute =[
            NSAttributedString.Key.StrokeColor: color
        ]
    }
    
    func getStringFromCard(_ card: Card) -> String {
        var shape = ""
        switch card.shape {
        case .diamond:
            shape = String(repeating:"", count: card.numberOfShape.rawValue)
        case .squiggle:
            shape = String(repeating:"", count: card.numberOfShape.rawValue)
        case.stadium:
            shape = String(repeating:"", count: card.numberOfShape.rawValue)
        }
        return shape
    }
    
    func getUIColorFromCard(_ card: Card) -> UIColor {
        let color: UIColor
        switch card.color {
        case .green:
            return UIColor.green
        case .purple:
            return UIColor.purple
        case .red:
            return UIColor.red
        }
    }
    
    func setNoCardButton(_ button: UIButton) {
        button.setTitle
    }
}

