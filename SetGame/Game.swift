//
//  Game.swift
//  SetGame
//
//  Created by candela on 2019/4/18.
//  Copyright © 2019年 candela. All rights reserved.
//

import Foundation

struct Game {
    var cards = [Card]()
    var showingCards = [Card]()
    var selectedCards = [Card]()
    
    
    
    private mutating func initCards() {
        for numberOfShape in Card.NumbeOfShape.allCases {
            for shape in Card.Shape.allCases {
                for shading in Card.Shading.allCases {
                    for color in Card.Color.allCases {
                        cards.append(Card(numberOfShape: numberOfShape, shape: shape, shading: shading, color: color))
                        
                    }
                    
                }
            }
        }
    }
    
    func initShowingCards(numberOfCards: Int) {
        assert(numberOfCards <= cards.count, "Game.initShwoingCards(\(numberOfCards), not enough cards to show")
        
        for i in 0..numberOfCards {
            showingCards.append(cards[i])
        }
    }
    
    private mutating func shuffleCards {
        cards.shuffle()
    }
    
    func selectCards(card: Card) {
        if selectedCards.count <= 3 {
            if selectedCards.contains(card) {
                selectedCards.append(card)
            } else {
                if isSelectedCardsSet() {
                    score += Game.bonus
                } else {
                    score -= Game.penalty
                }
            }
        }
        

    }
}
